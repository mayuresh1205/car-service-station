package HibernateClasses;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


public class StoreData {
    public static void main(String[] args) {
        StandardServiceRegistry ssr = null;
        System.out.println("Hello");
        try {
             ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

        SessionFactory factory = meta.getSessionFactoryBuilder().build();
        Session session = factory.openSession();
        Transaction t = session.beginTransaction();

        Customer c1 = new Customer();
        c1.setName("Mayuresh");
        c1.setPhoneNo("8308481156");
        c1.setCarId(1);

        //Customer c2 = (Customer) session.get(Customer.class,c1.getId());
        
        session.save(c1);
        t.commit();
        System.out.println("successfully saved");


        factory.close();
        session.close();

    }
}