package HibernateClasses;
import javax.persistence.*;

@Entity
public class Customer {

    @Id
    private int Id;

    @Column
    private String Name;

    @Column
    private  String phoneNo;

    @Column
    private int CarId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int carId) {
        CarId = carId;
    }


}
