package carService;

import customer.Car;
import customer.Customer;
import manager.*;
import repo.CarRepository;
public class CarService {

	public static void main(String[] args) {

		System.out.println("Car Service Station Activated\n");
		boolean serviceStatus = false,fuelStatus= false, washStatus=false;
		
		CarRepository carRepository = new CarRepository();
		Car car = new Car("SUV","MH 12 1000",40,20);
		int id = carRepository.saveCar(car);

		Car newCar = carRepository.getCar(id);
		
		Customer customer = new Customer("Mayuresh Pingale","9999999999",newCar);
		
		System.out.println("Customer Name: " + customer.getCustomerName());
		System.out.println("Customer No  : " + customer.getPhoneNumber());
		System.out.println("Car Type     : " + customer.car.getCarType());
		System.out.println("Car No       : " + customer.car.getNumber());
		System.out.println("Car Id: " + id);
		
		CarServiceManager serviceManager = new CarServiceManager("Jack");
		System.out.println("\nService Manager Name: " + serviceManager.getManagerName());
		try {
			serviceManager.setIsAllPartsAvailable(true);
			serviceStatus = serviceManager.service();
		} catch (PartsNotAvailable e1) {
			System.out.println("Service Parts Not Available");
		}
		finally {
			System.out.println("Servicing Completed");
		}
		
		FuelManager fuelManager = new FuelManager("Jill");
		System.out.println("\nFuel Manager Name: " + fuelManager.getManagerName());
		try {
			fuelStatus = fuelManager.fuel(car);
		} 
		catch (FuelNotAvailableException e) {
			System.out.println("Enough Fuel Not Available");
		}
		catch(TankFilledException e) {
			System.out.println("Tank Already Filled");
		}
		finally {
			System.out.println("Fueling Completed");	
		}
		
		WashingManager washingManager = new WashingManager("Hill");
		System.out.println("\nWashing Manager Name: " + washingManager.getManagerName());
		try {
		washingManager.setisWaterAvailable(false);
		washStatus = washingManager.wash();
		}
		catch (WaterNotAvailablExecption e) {
			System.out.println("Water Not Available");
		}
		finally {
			System.out.println("Washing Completed");
		}
		
		generateReport(serviceStatus, fuelStatus, washStatus);
	}
	
	static void generateReport(boolean serviceStatus, boolean fuelStatus, boolean washStatus) {
		
		System.out.println("\n\nFinal Report");
		System.out.println("Servicing Performed: " + serviceStatus);
		System.out.println("Fuel Performed: " + fuelStatus);
		System.out.println("Washing Performed: " + washStatus);
		
	}
	
	

}
