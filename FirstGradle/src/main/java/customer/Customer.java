package customer;

public class Customer {
	String customerName;
	String phoneNumber;
	public Car car;
	
	public Customer(String customerName,String phoneNumber,Car car) {
		this.customerName = customerName;
		this.phoneNumber = phoneNumber;
		this.car = car;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String name) {
		customerName = name;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String number) {
		phoneNumber = number;
	}


}
