package customer;

public class Car {
	String type;
	String number;
	int carCapacity;
	int availableFuel;
	
	public Car(String type,String number,int carCapicity, int availableFuel) {
		this.type = type;
		this.number = number;
		this.carCapacity = carCapicity;
		this.availableFuel = availableFuel;
	}
	
	public String getCarType() {
		return type;
	}
	
	public void setCarType(String type) {
		this.type = type;
	}
	
	public int getAvailabaleFuel() {
		return availableFuel;
	}
	
	public void setAvailableFuel(int availableFuel) {
		this.availableFuel = availableFuel;
	}
	
	public int getCarCapacity() {
		return carCapacity;
	}
	
	public void setCarCapacity(int carCapacity) {
		this.carCapacity = carCapacity;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
	
	

}
