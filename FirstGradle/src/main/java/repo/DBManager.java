package repo;

import customer.Car;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DBManager {
    static final String DB_URL = "jdbc:mysql://localhost:3306/mechanic";
    static final String USER = "root";
    static final String PASS = "PinMysql@1234";
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, USER, PASS);
    }

    public static void main(String[] args) {
        CarRepository carRepository = new CarRepository();
        Car car = carRepository.getCar(1);
        System.out.println(car);
    }
}
