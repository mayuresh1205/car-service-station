package repo;

import customer.Car;

import java.sql.*;

public class CarRepository {
    static final String QUERY = "select id , number, type, capacity,availableFuel from car ";
    public Car getCar(int id) {
        try(Connection conn = DBManager.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(QUERY);) {


            // Extract data from result set
            if (rs.next()) {
                System.out.print("ID: " + rs.getInt("id"));
                System.out.print(", number: " + rs.getString("number"));
                System.out.print(", type: " + rs.getString("type"));
                System.out.println(", capacity: " + rs.getInt("capacity"));
                return new Car(rs.getString("type"),rs.getString("number"),rs.getInt("capacity"),rs.getInt("availableFuel"));
                // Retrieve by column name
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }
    public int saveCar(Car car) {
        String query = "insert into car (number,type , capacity, availablefuel) values ('%s' , '%s' , %d, %d);";
        String executableQuery = String.format(query, car.getNumber(), car.getCarType(), car.getCarCapacity(), car.getAvailabaleFuel());
        try(Connection conn = DBManager.getConnection();
            Statement stmt = conn.createStatement();
            ) {
            return stmt.executeUpdate(executableQuery);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
