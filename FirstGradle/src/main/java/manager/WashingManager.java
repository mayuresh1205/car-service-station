package manager;

public class WashingManager extends Manager{
	boolean isWaterAvailable;
	
	public WashingManager(String name) {
		super(name);
	}
	
	public boolean getisWaterAvailable() {
		return isWaterAvailable;
	}
	
	public void setisWaterAvailable(boolean isWaterAvailable) {
		this.isWaterAvailable = isWaterAvailable;
	}
	
	public boolean wash() throws WaterNotAvailablExecption {
		System.out.println("Washing the Car");
		
		if(!isWaterAvailable)
			throw new WaterNotAvailablExecption();
		
		System.out.println("Washing Successfull");
		return true;
	}
	

}
