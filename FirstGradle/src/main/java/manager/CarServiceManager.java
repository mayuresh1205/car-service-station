package manager;

public class CarServiceManager extends Manager {

	boolean isAllPartsAvailable;
	
	
	public CarServiceManager(String name) {
		super(name);
	}
	
	
	public boolean getIsAllPartsAvailable() {
		return isAllPartsAvailable;
	}
	
	public void setIsAllPartsAvailable(boolean isAllPartsAvailable) {
		this.isAllPartsAvailable = isAllPartsAvailable; 
	}
	
	public boolean service() throws PartsNotAvailable {
		System.out.println("Servicing Started");
		
		if(!isAllPartsAvailable)
			throw new PartsNotAvailable();
		
		System.out.println("Servicing Successfull");
		return true;
	}
	
	
	
	
	

}
