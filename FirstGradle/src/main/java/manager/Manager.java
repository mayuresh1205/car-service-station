package manager;

public class Manager {

	String name;
	
	public Manager(String name) {
		this.name = name;
	}
	
	public String getManagerName() {
		return name;
	}
	
	public void setManagerName(String name) {
		 this.name = name;
	}
	
}
