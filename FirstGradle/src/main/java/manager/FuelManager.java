package manager;
import customer.Car;

public class FuelManager extends Manager{
	int availableFuel = 100;

	
	public FuelManager(String name) {
		super(name);
	}
	
	public boolean fuel(Car car) throws FuelNotAvailableException, TankFilledException {
		System.out.println("Fueling The Car");
		int requiredFuel =  car.getCarCapacity() - car.getAvailabaleFuel();
		
		if(requiredFuel > availableFuel )
			throw new FuelNotAvailableException();
		else if(requiredFuel == 0)
			throw new TankFilledException();

		System.out.println("Total Fuel Required: "+ requiredFuel);
		System.out.println("Fueling Sucessfull");	
		return true;
		
	}
	

}
